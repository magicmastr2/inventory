@extends('app')

@section('content')

<p> First Name: {{ $customer->first_name }}</p>
<p> Last Name: {{ $customer->last_name }}</p>
<p> Email: {{ $customer->email }}</p>
<p> Address: {{ $customer->address }}</p>
<p> Phone Number: {{ $customer->phone }}</p>
<p> Username: {{ $customer->username }}</p>
<p> Password: {{ $customer->password }}</p>

@endsection