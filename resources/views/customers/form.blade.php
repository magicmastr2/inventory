<div class="row">
    <div class="col-md-3">
      {!! Form::label('First Name') !!}
      {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
      </div>
    <div class="col-md-3">
      {!! Form::label('Last Name') !!}
      {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
      </div>
    <div class="col-md-3">
      {!! Form::label('Email') !!}
      {!! Form::text('email', null, ['class' => 'form-control']) !!}
      </div>
    <div class="col-md-3">
      {!! Form::label('Phone') !!}
      {!! Form::text('phone', null, ['class' => 'form-control']) !!}
      </div>
    <div class="col-md-3">
      {!! Form::label('Address') !!}
      {!! Form::text('adress', null, ['class' => 'form-control']) !!}
      </div>
    <div class="col-md-3">
      {!! Form::label('Username') !!}
      {!! Form::text('username', null, ['class' => 'form-control']) !!}
      </div>
    <div class="col-md-3">
      {!! Form::label('Password') !!}
      {!! Form::text('password', null, ['class' => 'form-control']) !!}
      </div>
</div>

<div class="row">
   <div class="col-md-3">
     <p>{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}</p>
    </div>
</div>