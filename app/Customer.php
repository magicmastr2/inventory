<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
      'account_number',
      'first_name',
      'last_name',
      'username',
      'email',
      'password',
      'phone',
      'address'
    ];
}
