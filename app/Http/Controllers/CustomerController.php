<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CustomerController extends Controller
{
    public function create() {
      return view ('customers.create');
    }
    public function store(Requests\CustomerRequest $request) {
    $customer = Customer::create($request->all());
    return redirect()->route('customers.show', $customer->id);
    }
    public function show($id) {
    $customer = Customer::find($id);
    return view('customers.show', compact('customer')); }
}